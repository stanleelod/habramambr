$(function() {
	var add_post = $('#add_post'),
		add_post_form = $('#add_post_form');
	
	add_post_form.hide();
    add_post.click(function() {
    	add_post_form.slideToggle(500);
        return false;
    });

    function removeAddForm () {
    	add_post_form.slideUp(500);
    };

    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    
    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

    $('#close').click(function(){
        removeAddForm();
        return false;
    });

    $('.delete').click(function() {
        var id = $(this).attr("data-id");
        $.ajax({
            url: '../script/delete.php',
            type: "POST",
            data: {"id": id},
        })
        .done(function() {
            location.reload();
        });
        return false;
    });
    $('.crud .post').click(function() {
        var id = $(this).attr("data-post");
        window.location.replace('post.php?id='+id);
    });
});